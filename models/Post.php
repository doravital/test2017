<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property string $title
 * @property string $body
 * @property string $category
 * @property string $author
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @property string $updated_by
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'body', 'category', 'author', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'required'],
            [['title', 'body', 'category', 'author', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'body' => 'Body',
            'category' => 'Category',
            'author' => 'Author',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);

        if ($this->isNewRecord)
		    $this->created_at = 'avi';
		
		if ($this->isNewRecord)
		    $this->updated_at = 'meir';
		
		if ($this->isNewRecord)
		    $this->created_by = 'dana';
		
		if ($this->isNewRecord)
		    $this->updated_by = 'kfir';
		
        return $return;
    }	
}
