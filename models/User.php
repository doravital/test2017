<?php

namespace app\models;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use Yii;

class User extends ActiveRecord implements \yii\web\IdentityInterface
{
	public $role;
	
	public static function tableName()
    {
        return 'user';
    }
	
	public function rules()
	{
		return
		
		
		[
			[['username', 'password', 'auth_key','name'],'string', 'max' =>255],
			[['username', 'password',], 'required'],
			[['username'], 'unique'],
			['role', 'safe'],
		];
	}

    /**
     * @inheritdoc
     */
    public static function findIdentity($id) /** מציג את כל הפרטים של האובייקט לפי האיידי**/
    {
        return self::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
       throw new NotSupportedException('Not supported');
	   
	   return null;
        
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
       return self::findOne(['username' => $username]);
        

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
 public function validatePassword($password)
    {
        return $this->isCorrectHash($password, $this->password);
    }
	
	private function isCorrectHash($plaintext, $hash)
	{
		return Yii::$app->security->validatePassword($plaintext, $hash);
	}
	public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);

        if ($this->isAttributeChanged('password'))
            $this->password = Yii::$app->security->
					generatePasswordHash($this->password);

        if ($this->isNewRecord)
		    $this->auth_key = Yii::$app->security->generateRandomString(32);

        return $return;
    }	
	//create fullname pseuodo field
	public function getFullname()
    {
        return $this->firstname.' '.$this->lastname;
    }
	//A method to get an array of all users models/User
	public static function getUsers()
	{
		$users = ArrayHelper::
					map(self::find()->all(), 'id', 'fullname');
		return $users;						
	}

}
