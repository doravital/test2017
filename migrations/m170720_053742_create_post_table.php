<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post`.
 */
class m170720_053742_create_post_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('post', [
            'id' => $this->primaryKey(),
			'title' => $this->string()->notNull(),
			'body' => $this->string()->notNull(),
			'category' => $this->string()->notNull(),
			'author' => $this->string()->notNull(),
			'status' => $this->string()->notNull(),
			'created_at' => $this->string()->notNull(),
			'updated_at' => $this->string()->notNull(),
			'created_by' => $this->string()->notNull(),
			'updated_by' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('post');
    }
}
